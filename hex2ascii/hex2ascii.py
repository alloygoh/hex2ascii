#!/usr/bin/env python3
import sys
import binascii

def main():
    if len(sys.argv) != 2 :
        raise ValueError("Please provide a hex value to convert.")
    
    hex_string = sys.argv[1]

    try:
        ascii_str = binascii.unhexlify(hexconvert) 
    except TypeError:
        ascii_str = "Not a valid hex string"


    return ascii_str
