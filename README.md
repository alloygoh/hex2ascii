# hex2ascii

This is a tool to convert Hex characters to Ascii

## Getting Started
git clone https://gitlab.com/alloygoh/hex2ascii.git

## Prerequisites

Python3

pip for python3

### Installing
To install, run the following commands
```
$ cd hex2ascii

$ sudo pip install .
```


### Usage
```
$ hex2ascii <hex character to convert>
```

### Uninstalling

To uninstall, simply run ```pip uninstall hex2ascii```

## Author
* **Alloysius Goh** - *Initial work* 
